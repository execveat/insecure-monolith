// Unsafe regexp
var emailExpression = /^([a-zA-Z0-9_.\-])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var email = "john@example.com";
emailExpression.test(email);